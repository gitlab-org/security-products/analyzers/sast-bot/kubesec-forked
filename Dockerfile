ARG SCANNER_VERSION=2.14.2
ARG SCANNER_BIN_SHA256=bc252e35f01bc4f133a49404315da3ccfed0209cc9baba33883eaeca0656f35c

FROM alpine/helm:3.9.0 AS helm

FROM golang:1.22.3-alpine AS build
ARG SCANNER_VERSION
ARG SCANNER_BIN_SHA256
ENV CGO_ENABLED=0
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

# The "kubesec" user (from the kubesec/kubesec image) doesn't have permission to create a
# file in /etc/ssl/certs, this RUN command creates files that the
# analyzer can modify.
RUN touch /ca-certificates.crt

ADD https://github.com/controlplaneio/kubesec/releases/download/v${SCANNER_VERSION}/kubesec_linux_amd64.tar.gz /tmp/kubesec.tar.gz
RUN echo "$SCANNER_BIN_SHA256  /tmp/kubesec.tar.gz" | sha256sum -c
RUN tar -xf /tmp/kubesec.tar.gz
RUN mv kubesec /tmp/kubesec

FROM alpine:3.16.2
USER root
RUN apk update && apk add git apk-tools=2.12.9-r3 && apk upgrade

RUN addgroup -g 1000 gitlab && adduser -u 1000 -G gitlab -S -D gitlab 
USER gitlab

ARG SCANNER_VERSION
ENV SCANNER_VERSION $SCANNER_VERSION

COPY --from=helm /usr/bin/helm /usr/bin/helm
ENV PATH="/home/app:${PATH}"
COPY --from=build --chown=gitlab:gitlab /go/src/app/analyzer /
COPY --from=build --chown=gitlab:gitlab /tmp/kubesec /bin/kubesec
COPY --from=build --chown=gitlab:gitlab /ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

ENTRYPOINT []
CMD ["/analyzer", "run"]
