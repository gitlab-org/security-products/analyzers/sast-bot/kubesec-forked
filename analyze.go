package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os/exec"
	"path/filepath"
	"syscall"

	"github.com/kballard/go-shellquote"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v5/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v5/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v5/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

const (
	pathOutput           = "kubesec.json"
	helmManifestFilename = "sast_helm_generated_manifest.yaml"
)

var vulnerabilities = make([]convert.KubesecOutput, 0)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    "helm-charts-path",
			Usage:   "Optional helm charts path where to run helm on before runing kubesec",
			EnvVars: []string{"KUBESEC_HELM_CHARTS_PATH"},
		},
		&cli.StringFlag{
			Name:    "helm-options",
			Usage:   "Specify helm options",
			EnvVars: []string{"KUBESEC_HELM_OPTIONS"},
		},
	}
}

func analyze(c *cli.Context, repositoryPath string, _ *ruleset.Config) (io.ReadCloser, error) {
	var err error
	var manifests []string

	var helmChartsPath string = c.String("helm-charts-path")
	if helmChartsPath != "" {
		log.Infof("Searching %s for Helm lock file", helmChartsPath)
		if plugin.IsHelmProject(helmChartsPath) {
			prepareHelmProject()
		}
		var args []string

		args = append(args, "template", helmChartsPath)
		if c.String("helm-options") != "" {
			helmOptions, err := shellquote.Split(c.String("helm-options"))
			if err != nil {
				log.Errorf("Invalid helm options:\n%s", err)
				return nil, err
			}
			args = append(args, helmOptions...)
		}
		cmd := exec.Command("helm", args...)

		cmd.Dir = repositoryPath
		stdout, stderr, err := run(cmd)
		combined := fmt.Sprintf("%s\n%s", stderr, stdout)
		log.Debugf("%s\n%s", cmd.String(), combined)
		if err != nil {
			log.Errorf("An error occurred while running helm template:\n%s", combined)
			return nil, err
		}

		manifest := filepath.Join(repositoryPath, helmManifestFilename)

		err = ioutil.WriteFile(manifest, stdout, 0600)
		if err != nil {
			log.Errorf("cannot write file: %s\n%s", manifest, stdout)
			return nil, err
		}
		manifests = []string{fmt.Sprintf("/%s", helmManifestFilename)}
		log.Debugf("Creating helm manifest: %s", helmManifestFilename)
	} else {
		log.Infof("Searching %s for Kubernetes manifests.\n", repositoryPath)
		manifests = plugin.WalkForYaml(repositoryPath)
	}

	outputs := make(chan convert.KubesecOutput)
	for i := range manifests {
		manifestPath := manifests[i]
		go scanManifest(repositoryPath, manifestPath, outputs)
	}

	for i := 0; i < len(manifests); i++ {
		select {
		case vuln := <-outputs:
			vulnerabilities = append(vulnerabilities, vuln)
		}
	}

	file, err := json.MarshalIndent(vulnerabilities, "", " ")

	return ioutil.NopCloser(bytes.NewReader(file)), err
}

func prepareHelmProject() error {
	args := []string{"dependency", "build"}
	cmd := exec.Command("helm", args...)
	_, _, err := run(cmd)

	if err != nil {
		return err
	}

	return nil
}

func scanManifest(path string, manifestPath string, outputs chan<- convert.KubesecOutput) {
	output := convert.KubesecOutput{
		Filepath: manifestPath,
	}

	args := []string{
		"scan",
		path + manifestPath,
	}

	cmd := exec.Command("kubesec", args...)
	stdout, stderr, err := run(cmd)
	log.Debugf("%s\n%s\n%s", cmd.String(), stderr, stdout)

	output.Findings = stdout

	// This command results in an an exit status 2,
	// but it does return the json as expected.
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			waitStatus := exitError.Sys().(syscall.WaitStatus)
			if waitStatus.ExitStatus() != 2 {
				// An error occurred while running kubesec
				// (see https://github.com/controlplaneio/kubesec/blob/74a92d6ab378f8334ef4140c7bd5caeb4f22a265/cmd/kubesec/main.go#L30 )
				log.Errorf("An error occurred while running kubesec:\n%s", err)

				output.Error = err
			}
		}
	}

	outputs <- output
}

func loadRulesetConfig(projectPath string) (*ruleset.Config, error) {
	// Load custom config if available
	rulesetPath := filepath.Join(projectPath, ruleset.PathSAST)
	return ruleset.Load(rulesetPath, metadata.AnalyzerID, log.StandardLogger())
}

func run(cmd *exec.Cmd) ([]byte, []byte, error) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	return stdout.Bytes(), stderr.Bytes(), err
}
