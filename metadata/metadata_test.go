package metadata_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v5/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "kubesec",
		Name:    "Kubesec",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://github.com/controlplaneio/kubesec",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
